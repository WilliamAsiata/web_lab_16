
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by wasia on 23/05/2017.
 */
public class ImageGallerySubmit extends HttpServlet {

    private boolean isMultipart;
    private String filePath;
    private String photoFolder = "/Uploaded-Photos/";
    private int maxFileSize = 99999 * 1024;
    private int maxMemSize = 99999 * 1024;
    private File file ;

    public void init( ){
        // Get the file location where it would be stored.
        filePath = getServletContext().getRealPath(photoFolder);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // Check that we have a file upload request
        isMultipart = ServletFileUpload.isMultipartContent(request);

        response.setContentType("text/html");

        PrintWriter out = response.getWriter( );

        if(!isMultipart){
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet upload</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<p>No file uploaded</p>");
            out.println("</body>");
            out.println("</html>");
            return;
        }

        DiskFileItemFactory factory = new DiskFileItemFactory();

        // maximum size that will be stored in memory
        factory.setSizeThreshold(maxMemSize);

        // Location to save data that is larger than maxMemSize.
        factory.setRepository(new File("c:\\temp"));

        // Create a new file upload handler
        ServletFileUpload upload = new ServletFileUpload(factory);

        // maximum file size to be uploaded.
        upload.setSizeMax( maxFileSize );

        try{
            // Parse the request to get file items.
            List fileItems = upload.parseRequest(request);

            // Process the uploaded file items
            Iterator i = fileItems.iterator();

            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet upload</title>");
            out.println("</head>");
            out.println("<body>");

            out.println("<table>");

            while (i.hasNext()) {

                FileItem fi = (FileItem) i.next();

                if (!fi.isFormField()) {

                    // Get some uploaded file parameters
                    String fileName = fi.getName();
                    String fileType = fileName.substring(fileName.lastIndexOf("."), fileName.length());

                    // Write the file
                    file = new File( filePath + "fullsize" + fileType);

                    fi.write(file);

                    BufferedImage img = new BufferedImage(100, 100, BufferedImage.TYPE_INT_RGB);
                    img.createGraphics().drawImage(ImageIO.read(file).getScaledInstance(100, 100, Image.SCALE_SMOOTH),0,0,null);
                    ImageIO.write(img, "png", new File(filePath + "thumbnail.png"));

                    out.println("");

                    out.println("<tr>");

                    out.println("<th>Uploaded Filename: " + fileName + "<br>" + file.length() + " bytes</th>");
                    out.println("<td><a href=\"" + photoFolder + "fullsize" + fileType + "\"><img src=\"" + photoFolder + "thumbnail.png\"></a></td>");

                    out.println("</tr>");
                }

            }

            out.println("</table>");

            out.println("</body>");
            out.println("</html>");

        } catch(Exception ex) {System.out.println(ex);}
    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws ServletException, java.io.IOException {

        throw new ServletException("GET method used with " +
                getClass( ).getName( )+": POST method required.");
    }
}