import org.json.simple.JSONObject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.util.*;

/**
 * Servlet implementation class RegisterUser
 */
public class RegisterUser extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private String[] keys = new String[]{"firstname","lastname","username","email"};
    protected String root;

    public void init( ){
        // Get the file location where it would be stored.
        root = getServletContext().getRealPath("/");
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter out = response.getWriter();

        out.println("<!doctype html>");
        out.println("<html>");
        out.println("<head>");
        out.println("<title>Register users</title>");
        out.println("<meta charset='UTF-8' />");
        out.println("</head>");
        out.println("<body>");

        HttpSession session = request.getSession();
        Enumeration<String> params = request.getParameterNames();

        if (params.hasMoreElements()) {
            // This is the result of either a submit form, or clear form
            int numFieldsFilledIn = storeFormDataSessionToJSON(request, out);

            if (numFieldsFilledIn == 0)
                out.println("<p>No information entered.</p><p>To return to the registration page, click <a href='Register'>here</a>.</p>");

            else if (numFieldsFilledIn != keys.length)
                out.println("<p>The data you've entered so far has been saved.</p><p>To continue your registration click <a href='Register'>here</a>.</p>");

            else out.println("<p>You have been registered!</p>");
        } else createForm(session, out);

        // close off the HTML page
        out.println("</body></html>");
    }


    protected int storeFormDataSessionToJSON(HttpServletRequest request, PrintWriter out) {

        // Needs to be completed!
        int count = 0;

        // Store the three form fields as attributes in the session
        // (for the ones that exist)
        // Return a count of how many of the fields were stored

        File jsonFile = new File(root,request.getRequestedSessionId()+".json");

        System.out.println("storing data");

        JSONObject sessionJSON = new JSONObject();

        for (String key: keys) {
            String value = request.getParameterValues(key)[0];
            if (!value.isEmpty()) {
                sessionJSON.put(key, value);
                count++;
            }
        }

        if (saveJSONObject(jsonFile, sessionJSON, out)) out.println("<p>JSON file successfully saved to location = " + jsonFile + "</p>");
        else out.println("<p><a href=\"Register\">Try again</a></p>");

        return count;
    }

    protected void createForm(HttpSession session, PrintWriter out) throws FileNotFoundException {

        System.out.println("creating form");

        boolean complete = false;
        Map<String,String> formFields = new HashMap<>();

        File jsonFile = new File(root, session.getId() + ".json");
        if (jsonFile.exists()){
            System.out.println(jsonFile.toPath());
            Scanner scanner = new Scanner(jsonFile);

            String jsonString = scanner.nextLine().replace("\"","");
            
            String[] jsonEntries = jsonString.substring(1, jsonString.length()-1).split(",");

            for (String entry: jsonEntries) {
                String[] pair = entry.split(":");
                formFields.put(pair[0],pair[1]);
            }
            complete = keys.length == formFields.size();
        }

        out.println("<form style='width:500px;margin:auto;' id='userform_id' name='userform' method='get' action='Register'>");
        out.println("<fieldset><legend>Register as a new user:</legend>");

        // Generate appropriate input form fields & 'Pre-fill' out a particular field in the form
        // if it exists in the variable 'formFields':

        String readOnly = "";
        if (complete) readOnly = "readonly";
        for (String key: keys) {

            String type = "text";
            String labelText = "";
            String placeHolder = "";
            String pattern = "";
            switch (key){
                case "firstname":
                    placeHolder = "first name";
                    labelText = "First name";
                    break;
                case "lastname":
                    placeHolder = "last name";
                    labelText = "Last name (no digits)";
                    pattern = "pattern=\"^([^0-9]*)$\"";
                    break;
                case "username":
                    placeHolder = key;
                    labelText = "Username";
                    break;
                case "email":
                    type = key;
                    placeHolder = key;
                    labelText = "Your " + key;
                    break;
            }
            // Get session data
            String value = "";
            if (formFields.containsKey(key)) value = formFields.get(key);

            out.println("<p>");
            out.println("<label for=\""+key+"\">" + labelText + ":</label>");
            out.println("<br>");
            out.println("<input type=\""+type+"\" id=\""+key+"\" name=\""+key+"\" "+pattern+readOnly+" placeholder=\"Your "+placeHolder+" goes here\" value=\""+value+"\"/>");
            out.println("</p>");
        }

        if (!complete) out.println("<input type='submit' name='submit_button' id='submit_id' value='Register' /></p>");

        out.println("</fieldset>");
        out.println("</form>");
    }

    private boolean saveJSONObject(File file, JSONObject jsonRecord, PrintWriter out) {
        boolean statusOK = true;

        String json_string = JSONObject.toJSONString(jsonRecord);

        BufferedWriter writer = null;

        try {
            writer = new BufferedWriter(new FileWriter(file));
            writer.write(json_string);
        }
        catch (IOException e) {
            statusOK = false;
            out.println("<p>Write error.</p>");
        }
        finally {
            try {if (writer != null) writer.close();}
            catch (IOException e) {
                out.println("<p>Write error.</p>");
                statusOK = false;}
        }
        return statusOK;
    }


    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        doGet(request, response);
    }
}