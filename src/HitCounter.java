import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by wasia on 23/05/2017.
 */
public class HitCounter extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Cookie[] cookies = req.getCookies();
        Cookie cookieCounter = null;
        String cookieName = "hit-count";

        for (Cookie cookie: cookies) {
            if (cookie.getName().equals(cookieName)){
                cookieCounter = cookie;
            }
            //            System.out.println(cookie.getName());
        }

        if (cookieCounter == null){
            cookieCounter = new Cookie(cookieName,"0");
            cookieCounter.setMaxAge(-1);
            cookieCounter.setPath("/");
            resp.addCookie(cookieCounter);
        }

        //        System.out.println(cookieCounter.getName());

        if (req.getParameter("checkbox") != null){
            cookieCounter.setMaxAge(0);
        } else cookieCounter.setValue(1 + Integer.parseInt(cookieCounter.getValue()) + "");
        //        System.out.println(cookieCounter.getValue());

        resp.addCookie(cookieCounter);

        resp.sendRedirect("/exercise01.html");
    }
}