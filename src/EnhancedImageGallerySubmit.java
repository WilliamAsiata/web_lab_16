
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.*;
import java.util.List;

/**
 * Created by wasia on 23/05/2017.
 */
public class EnhancedImageGallerySubmit extends HttpServlet {

    private String filePath;
    private String photoFolder = "/Uploaded-Photos/";

    public void init( ){
        // Get the file location where it would be stored.
        filePath = getServletContext().getRealPath(photoFolder);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // Check that we have a file upload request
        boolean isMultipart = ServletFileUpload.isMultipartContent(request);

        response.setContentType("text/html");

        if(!isMultipart){
            response.getWriter().println("<html><head><title>Servlet upload</title></head><body><p>No file uploaded</p></body></html>");
            return;
        }

        DiskFileItemFactory factory = new DiskFileItemFactory();

        // maximum size that will be stored in memory
        int maxMemSize = 99999 * 1024;
        factory.setSizeThreshold(maxMemSize);

        // Location to save data that is larger than maxMemSize.
        factory.setRepository(new File("c:\\temp"));

        // Create a new file upload handler
        ServletFileUpload upload = new ServletFileUpload(factory);

        // maximum file size to be uploaded.
        int maxFileSize = 99999 * 1024;
        upload.setSizeMax(maxFileSize);

        try{
            // Parse the request to get file items.
            List postItems = upload.parseRequest(request);

            // Get session info and create session dir
            String dirPath = filePath + request.getSession(true).getId() + "/";
            File sessionDir = new File(dirPath);
            if (!sessionDir.exists()) sessionDir.mkdir();

            // Process the uploaded file items
            Iterator i = postItems.iterator();

            while (i.hasNext()) {
                FileItem fileItem = (FileItem) i.next();
                if (!fileItem.isFormField()) {

                    // Get some uploaded file parameters
                    String fileName = fileItem.getName();
                    String fileType = fileName.substring(fileName.lastIndexOf("."), fileName.length());

                    // Write the file
                    File file = new File(dirPath + "fullsize" + fileType);

                    fileItem.write(file);

                    BufferedImage img = new BufferedImage(100, 100, BufferedImage.TYPE_INT_RGB);
                    img.createGraphics().drawImage(ImageIO.read(file).getScaledInstance(100, 100, Image.SCALE_SMOOTH),0,0,null);
                    ImageIO.write(img, "png", new File(dirPath + "thumbnail.png"));

                } else if (fileItem.getFieldName().equals("caption")) {

                    // Make caption.txt in session folder
                    PrintWriter pw = new PrintWriter(new FileWriter(new File( dirPath + "caption.txt")));
                    pw.println(fileItem.getString());
                    pw.close();
                }
            }

            // Print new HTML page
            doGet(request, response);

        } catch (Exception ex) {System.out.println(ex);}
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String dir_name = request.getSession(true).getId() + "/";

        File sessionDir = new File(filePath + dir_name);

        dir_name = photoFolder + dir_name;

        if (sessionDir.exists()){

            File[] listOfFiles = sessionDir.listFiles();

            if (0 < listOfFiles.length){

                PrintWriter out = response.getWriter();

                out.println("<html><head><title>Server response</title></head><body><table>");

                for (File file: listOfFiles) {
                    if (file.getName().endsWith(".txt")){
                        // Read txt and insert caption
                        Scanner scanner = new Scanner(file);
                        out.println("<tbody><tr><th>" + scanner.nextLine() + "</th></tr></tbody>");
                        scanner.close();

                    } else if (file.getName().startsWith("fullsize")){
                        out.println("<thead><tr>");

                        // Update dir_name
                        out.println("<td><a href=\"" + dir_name + file.getName() + "\"><img src=\"" + dir_name + "thumbnail.png\"></a></td>");

                        out.println("</tr></thead>");
                    }
                }

                out.println("</table></body></html>");

            } else request.getRequestDispatcher("exercise03.html").forward(request, response);

        } else request.getRequestDispatcher("exercise03.html").forward(request, response);
    }
}